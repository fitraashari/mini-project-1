<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peserta extends Model
{
    //
    protected $guarded = [];
    protected $table = "peserta";

    protected $fillable = [
        'nama_lengkap', 'jenis_kelamin', 'tanggal_lahir', 'alamat', 'nama_kontak_darurat', 'no_telp_darurat'
    ];

    public function user()
    {
        return $this->morphOne(User::class, 'role');
    }
    public function pendaftaran()
    {
        return $this->hasMany('App\Pendaftaran');
    }
}
