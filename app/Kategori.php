<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = "kategori";

    protected $fillable = [
        'nama', 'deskripsi', 'tempat', 'waktu'
    ];
    public function pendaftaran()
    {
        return $this->hasMany('App\Pendaftaran');
    }
}
