<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    //
    protected $table = "pendaftaran";
    protected $guarded = [];
    protected $with = ['peserta', 'kategori'];

    public function peserta()
    {
        return $this->belongsTo('App\Peserta');
    }
    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }
}
