<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user=[
            'id'=>$this->id,
            'no_telp'=>$this->no_telp,
            'email'=>$this->email,
            'user_type'=>$this->role_type,
        ];
        if($this->role_type=="peserta"){
            $user['detail_peserta']=$this->role;
        }
        return $user;
    }
}
