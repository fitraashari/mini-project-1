<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        request()->validate([
            'new_password'=>['required','min:6']
        ]);
        auth()->user()->update([
            'password'=>bcrypt(request('new_password'))
        ]);
        return response()->json('Update Password Berhasil',200);
    }
}
