<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Peserta;
use App\User;
// use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        //dd($request->all());
        if(request('role_type')=='peserta'){
            $peserta = Peserta::create([
                'nama_lengkap'=>request('nama_lengkap'),
                'jenis_kelamin'=>request('jenis_kelamin'),
                'tempat_lahir'=>request('tempat_lahir'),
                'tanggal_lahir'=>request('tanggal_lahir'),
                'alamat'=>request('alamat'),
                'nama_kontak_darurat'=>request('nama_kontak_darurat'),
                'no_telp_darurat'=>request('no_telp_darurat'),
            ]);
            
            User::create([
                'no_telp'=>request('no_telp'),
                'email'=>request('email'),
                'password'=> bcrypt(request('password')),
                'role_id'=> $peserta->id,
                'role_type'=>request('role_type'),
                ]);
            }else{
                User::create([
                    'no_telp'=>request('no_telp'),
                    'email'=>request('email'),
                    'password'=> bcrypt(request('password')),
                    'role_type'=>request('role_type'),
            ]);
        }
        
        
        return response()->json('Registrasi Berhasil',200);
    }
}
