<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KategoriRequest;
use App\Http\Resources\KategoriResource;

use App\Peserta;
use App\Kategori;

class KategoriController extends Controller
{
    public function index()
    {
        return KategoriResource::collection(Kategori::paginate(5));
    }

    public function create(KategoriRequest $request)
    {

        $kategori = Kategori::create([
            'nama' => request('nama'),
            'deskripsi' => request('deskripsi'),
            'tempat' => request('tempat'),
            'waktu' => request('waktu')
        ]);

        $res = [
            'status' => 'berhasil',
            'data' => $kategori
        ];

        return response()->json($res);
    }

    public function update(KategoriRequest $request, $id)
    {
        $kategori = Kategori::find($id);

        $kategori->update([
            'nama' => request('nama'),
            'deskripsi' => request('deskripsi'),
            'tempat' => request('tempat'),
            'waktu' => request('waktu')
        ]);

        return new KategoriResource($kategori);
    }

    public function show($id)
    {
        $kategori = Kategori::find($id);

        return new KategoriResource(($kategori));
    }

    public function destroy($id)
    {
        $kategori = Kategori::find($id);

        $kategori->delete();

        return response()->json(['Kategori Deleted'], 200);
    }
}
