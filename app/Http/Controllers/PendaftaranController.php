<?php

namespace App\Http\Controllers;

use App\Http\Requests\PendaftaranRequest;
use App\Http\Resources\PendaftaranCollection;
use App\Http\Resources\PendaftaranResource;
use App\Kategori;
use App\Pendaftaran;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PendaftaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $kategori = Kategori::get();
        // $list=[];
        // foreach($kategori as $cat){
        //     $list[]=['kategori_id'=>$cat->id,'daftar_peserta'=>Pendaftaran::where('kategori_id',$cat->id)->get()];
        // }

        if (auth()->user()->role_type == 'admin') { //jika admin login tampilkan semua data
            $list = Pendaftaran::get();
        } else { //jika peserta login hanya tampilkan data peserta yg sedang login
            $list = Pendaftaran::where('peserta_id', auth()->user()->role_id)->get();
        }
        return new PendaftaranCollection($list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PendaftaranRequest $request)
    {
        //
        $daftar = Pendaftaran::firstOrcreate($this->save()); //firstorcreate agar peserta tidak dapat mendaftar kategori yang sama 2x
        return response()->json(compact('daftar'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pendaftaran $pendaftaran)
    {
        //
        return new PendaftaranResource($pendaftaran);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PendaftaranRequest $request, Pendaftaran $pendaftaran)
    {
        //
        //cek apakah user yang mengupdate staatusnya adalah admin atau user yang mendaftar
        if (auth()->user()->role_type == 'admin' || auth()->user()->role_id == $pendaftaran->peserta_id) {
            $pendaftaran->update($this->save());
        } else { //jika peserta ingin mengupdate data pendaftaran peserta lain maka error 404
            return response(null, 403);
        }
        return new PendaftaranResource($pendaftaran);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pendaftaran $pendaftaran)
    {
        //
        if (auth()->user()->role_type == 'admin') { //jika admin yang login bisa hapus semua data
            $pendaftaran->delete();
            return response()->json('Pendaftaran Deleted', 200);
        } else {
            if (auth()->user()->role_id == $pendaftaran->peserta_id) { //jika peserta yang login hanya dapat menghapus data peserta sendiri
                $pendaftaran->delete();
                return response()->json('Pendaftaran Deleted', 200);
            } else {
                return response(null, 403);
            }
        }
    }

    public function save()
    {
        if (auth()->user()->role_type == 'admin') {
            $data = [
                'peserta_id' => request('peserta_id'),
                'kategori_id' => request('kategori_id'), 'status_pendaftaran' => request('status_pendaftaran')
            ];
        } else {
            $data = [
                'peserta_id' => auth()->user()->role_id,
                'kategori_id' => request('kategori_id'),
            ];
        }
        return $data;
    }
}
