<?php

namespace App\Http\Controllers;

use App\Http\Requests\PesertaRequest;
use App\Http\Resources\PesertaCollection;
use App\Peserta;
use App\User;
use Illuminate\Http\Request;

class PesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //jika admin tampilkan semua daftar peserta
        if(auth()->user()->role_type=='admin'){
            $peserta= Peserta::get();
        }else{//jika bukan tampilkan data peserta yang sedang login
            $peserta=auth()->user()->role;
        }
        return new PesertaCollection(compact('peserta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PesertaRequest $request)
    {
        //
        // $peserta = Peserta::create($this->storePeserta());
        // return response()->json(compact('peserta'),200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Peserta $peserta)
    {
        return new PesertaCollection(compact('peserta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PesertaRequest $request, Peserta $peserta)
    {
        //cek apakah user yang mengupdate staatusnya adalah admin atau user yang mendaftar
        if(auth()->user()->role_type=='admin'||auth()->user()->role_id==$peserta->id){
            $peserta->update($this->storePeserta());
            }else{ //jika peserta ingin mengupdate data peserta lain maka error 404
                return response(null,403);
        }
        return response()->json(compact('peserta'),200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   //hapus data peserta+akun usernya
        $peserta=Peserta::find($id);
        $user=User::where('role_id',$peserta->id);
        $peserta->delete();
        $user->delete();
        return response()->json('Peserta Berhasil Dihapus');
    }

    public function storePeserta(){
        $save = [
            'nama_lengkap'=>request('nama_lengkap'),
            'jenis_kelamin'=>request('jenis_kelamin'),
            'tempat_lahir'=>request('tempat_lahir'),
            'tanggal_lahir'=>request('tanggal_lahir'),
            'alamat'=>request('alamat'),
            'nama_kontak_darurat'=>request('nama_kontak_darurat'),
            'no_telp_darurat'=>request('no_telp_darurat'),
        ];
        return $save;
    }
}
