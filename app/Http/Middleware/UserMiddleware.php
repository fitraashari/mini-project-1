<?php

namespace App\Http\Middleware;

use Closure;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
        if($role == auth()->user()->role_type) return $next($request);

        $res = [
            'status' => 'error',
            'kode' => '403',
            'pesan' => 'Anda Tidak Memiliki Akses'
        ];

        return response($res,403);
    }
}
