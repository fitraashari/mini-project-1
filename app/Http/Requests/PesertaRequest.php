<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PesertaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_lengkap'=>['required','max:50'],
            'jenis_kelamin'=>['required','in:l,p'],
            'tanggal_lahir'=>['required','date'],
            'alamat'=>['required'],
            'nama_kontak_darurat'=>['required'],
            'no_telp_darurat'=>['required','numeric'],
        ];
    }
}
