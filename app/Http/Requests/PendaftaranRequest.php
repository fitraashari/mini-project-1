<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PendaftaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(auth()->user()->role_type=='admin'){
            return [
                'peserta_id'=>['required'],
                'kategori_id'=>['required'],
                'status_pendaftaran'=>['required','in:0,1']
            ];
        }else{
            return [
                'kategori_id'=>['required']
            ];
        }
    }
}
