<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_telp'=>['required','unique:users,no_telp','numeric','digits_between:7,20'],
            'email'=>['required','email','unique:users,email'],
            'password'=>['required','min:6'],
            'role_type'=>['required','in:peserta,admin'],
            'nama_lengkap'=>['required_if:role,peserta'],
            'jenis_kelamin'=>['required_if:role,peserta','in:l,p'],
            'tanggal_lahir'=>['required_if:role,peserta'],
            'alamat'=>['required_if:role,peserta'],
            'nama_kontak_darurat'=>['required_if:role,peserta'],
            'no_telp_darurat'=>['required_if:role,peserta','numeric'],
        ];
    }
}
