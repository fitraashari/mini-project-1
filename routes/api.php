<?php

// use Illuminate\Http\Request;

// /*
// |--------------------------------------------------------------------------
// | API Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register API routes for your application. These
// | routes are loaded by the RouteServiceProvider within a group which
// | is assigned the "api" middleware group. Enjoy building your API!
// |
// */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Auth
Route::namespace('Auth')->group(function () {
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

// Route Kategori

Route::middleware(['auth:api'])->group(function () {

    Route::middleware(['user:admin'])->group(function () {
        Route::post('kategori/create', 'KategoriController@create');
        Route::patch('kategori/{id}/update', 'KategoriController@update');
        Route::delete('kategori/{id}/delete', 'KategoriController@destroy');
        Route::get('peserta/{peserta}', 'PesertaController@show');
        //Route::post('peserta/create','PesertaController@store');
        Route::delete('peserta/{peserta}/delete', 'PesertaController@destroy');
    });

    Route::patch('pendaftaran/{pendaftaran}/update', 'PendaftaranController@update');
    Route::get('pendaftaran/{pendaftaran}', 'PendaftaranController@show');
    Route::delete('pendaftaran/{pendaftaran}/delete', 'PendaftaranController@destroy');
    Route::get('pendaftaran', 'PendaftaranController@index');
    Route::post('pendaftaran/create', 'PendaftaranController@store');
    Route::get('peserta', 'PesertaController@index');
    Route::patch('peserta/{peserta}/update', 'PesertaController@update');

    Route::get('user', 'UserController@home');
    Route::patch('change-pass', 'Auth\PasswordController');
});


Route::get('kategori', 'KategoriController@index');
Route::get('kategori/{id}', 'KategoriController@show');
