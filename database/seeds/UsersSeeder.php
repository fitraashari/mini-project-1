<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use App\Peserta;

use Faker\Factory as Faker;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 1; $i <= 10; $i++) {

            $peserta = Peserta::create([
                'nama_lengkap' => $faker->name,
                'jenis_kelamin' => $faker->randomElement(['l', 'p']),
                'tanggal_lahir' => $faker->dateTimeBetween('-30 years', 'now'),
                'alamat' => $faker->address,
                'nama_kontak_darurat' => $faker->firstName,
                'no_telp_darurat' => $faker->phoneNumber
            ]);

            DB::table('users')->insert([
                'role_id' => $peserta->id,
                'no_telp' => $faker->phoneNumber,
                'email' => $faker->email,
                'password' => '$2y$10$WsVqeGYk/z4nPM3dnBkgiurH.bEMm/H.Bv8/H1Q1TPnrEHFTrYnDu',
                'role_type' => 'peserta'
            ]);
        }
    }
}
