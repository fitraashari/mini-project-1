<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 1; $i <= 20; $i++) {

            DB::table('kategori')->insert([
                'nama' => $faker->sentence,
                'deskripsi' => $faker->paragraph,
                'tempat' => $faker->city,
                'waktu' => $faker->dateTimeBetween('now', '+2 years')
            ]);
        }
    }
}
